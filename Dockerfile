FROM registry.gitlab.com/atomic-vr/vvr/cicd/rust-builder:latest as builder

RUN USER=root cargo new --bin ada-bot
WORKDIR ./ada-bot
ADD ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
RUN cargo build --release --target=x86_64-unknown-linux-musl
RUN rm src/*.rs

ADD . ./

RUN rm ./target/x86_64-unknown-linux-musl/release/deps/ada_bot*
RUN cargo build --target=x86_64-unknown-linux-musl --release


FROM alpine:latest as runtime

ARG APP=/usr/src/app

EXPOSE 23456

ENV TZ=Etc/UTC \
    APP_USER=appuser \
    RUST_LOG=info

RUN addgroup -S $APP_USER \
    && adduser -S -g $APP_USER $APP_USER

RUN apk update \
    && apk add --no-cache ca-certificates tzdata \
    && rm -rf /var/cache/apk/*

COPY --from=builder /home/rust/src/ada-bot/target/x86_64-unknown-linux-musl/release/ada-bot ${APP}/ada-bot

RUN chown -R $APP_USER:$APP_USER ${APP}

USER $APP_USER
WORKDIR ${APP}

STOPSIGNAL SIGINT
CMD exec /usr/src/app/ada-bot