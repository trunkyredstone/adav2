use log::debug;
use serenity::model::channel::Message;

// Here's an example URL. First ID is guild, then channel, then message.
// https://canary.discord.com/channels/813849899451678780/868815073496408115/886688461439000586
pub fn message_to_url(message: Message) -> String {
    let guild_id = message.guild_id;
    if guild_id.is_none() {
        debug!("Guild ID was none");
        return "".to_string();
    }
    let guild_id = guild_id.unwrap().as_u64().to_string();

    let channel_id = message.channel_id.as_u64().to_string();
    let message_id = message.id.as_u64().to_string();

    let mut url = "https://canary.discord.com/channels/".to_string();
    url.push_str(&*guild_id);
    url.push_str("/");
    url.push_str(&*channel_id);
    url.push_str("/");
    url.push_str(&*message_id);

    url
}
