use log::{debug, info, warn};
use serenity::async_trait;
use serenity::client::{Context, EventHandler};
use serenity::model::channel::Message;
use serenity::model::event::MessageUpdateEvent;
use serenity::model::gateway::Ready;
use serenity::model::guild::Member;
use serenity::model::id::{ChannelId, GuildId, MessageId};
use serenity::model::user::User;
use serenity::utils::Color;

use crate::util::message_to_url;

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn guild_member_addition(&self, ctx: Context, _guild_id: GuildId, new_member: Member) {
        info!("Guild member addition");
        if let Err(e) = crate::config::welcome_channel()
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.title("Member Joined")
                        .description(format!(
                            "<@{}> - {}",
                            new_member.user.id, new_member.user.name
                        ))
                        .footer(|f| f.text(format!("ID: {}", new_member.user.id)))
                        .color(Color::DARK_GREEN)
                })
            })
            .await
        {
            warn!("Error sending message: {}", e);
        }
    }

    async fn guild_member_removal(
        &self,
        ctx: Context,
        _guild_id: GuildId,
        user: User,
        _member_data_if_available: Option<Member>,
    ) {
        info!("Guild member removal");
        if let Err(e) = crate::config::welcome_channel()
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.title("Member Left")
                        .description(format!("<@{}> - {}", user.id, user.name))
                        .footer(|f| f.text(format!("ID: {}", user.id)))
                        .color(Color::RED)
                })
            })
            .await
        {
            warn!("Error sending message: {}", e);
        }
    }

    async fn guild_member_update(
        &self,
        ctx: Context,
        old_if_available: Option<Member>,
        new: Member,
    ) {
        info!("Guild member update");

        if old_if_available.is_none() {
            debug!("Old wasn't available");
            return;
        } // Check if old is available
        let old = old_if_available.unwrap();

        if old.nick.is_none() && new.nick.is_none() {
            debug!("Nicks are both none");
            return;
        } // Check if a nickname has been applied/removed/changed

        if old.nick.is_some() && new.nick.is_some() {
            if old.nick.clone().unwrap() == new.nick.clone().unwrap() {
                debug!("Nicks are the same");
                return;
            } // Check if the applied nickname is the same
        }

        let user_id = new.user.id;

        let old_nick = match old.nick {
            None => "None".to_string(),
            Some(s) => s,
        };
        let new_nick = match new.nick {
            None => "None".to_string(),
            Some(s) => s,
        };

        if let Err(e) = crate::config::welcome_channel()
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.title("Nickname changed")
                        .description(format!("<@{}> nickname changed", user_id))
                        .field("Before", old_nick, false)
                        .field("After", new_nick, false)
                        .footer(|f| f.text(format!("ID: {}", user_id)))
                        .color(Color::ORANGE)
                })
            })
            .await
        {
            warn!("Error sending message: {}", e);
        }
    }

    async fn message_delete(
        &self,
        ctx: Context,
        channel_id: ChannelId,
        deleted_message_id: MessageId,
        _guild_id: Option<GuildId>,
    ) {
        info!("Message deleted");
        let message = ctx.cache.message(channel_id, deleted_message_id).await;

        if message.is_none() {
            info!("Message deleted wasn't in the cache");
        }
        let message = message.unwrap();

        let message_id = message.id;
        let message_author = message.author.id;

        if let Err(e) = crate::config::message_log_channel()
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.title("Message deleted")
                        .description(format!(
                            "Message sent by <@{}> deleted in <#{}>",
                            message_author, channel_id
                        ))
                        .field("Content", message.content, true)
                        .footer(|f| {
                            f.text(format!(
                                "Author: {} | Message ID: {}",
                                message_author, message_id
                            ))
                        })
                        .color(Color::RED)
                })
            })
            .await
        {
            warn!("Error sending message: {}", e);
        }
    }

    async fn message_update(
        &self,
        ctx: Context,
        old_if_available: Option<Message>,
        new: Option<Message>,
        _event: MessageUpdateEvent,
    ) {
        info!("Message update");

        if old_if_available.is_none() || new.is_none() {
            debug!("One message was none");
            return;
        }

        let new = new.unwrap();
        let old_if_available = old_if_available.unwrap();

        let new_clone = new.clone();
        let new_content = new_clone.content;
        let new_id = new_clone.id;
        let author_id = new_clone.author.id;
        let channel_id = new_clone.channel_id;

        if new_content == old_if_available.content.clone() {
            debug!("Message content was the same");
            return;
        }

        if let Err(e) = crate::config::message_log_channel()
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.title(format!("Message edited"))
                        .description(format!("In <#{}> by <@{}>", channel_id, author_id))
                        .field("Before", old_if_available.content, false)
                        .field("After", new_content, false)
                        .footer(|f| {
                            f.text(format!("Author: {} | Message ID: {}", author_id, new_id))
                        })
                        .color(Color::RED)
                        .url(message_to_url(new))
                })
            })
            .await
        {
            warn!("Error sending message: {}", e);
        }
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        ctx.cache.set_max_messages(1000).await;

        info!("{} is ready", ready.user.name);
    }
}
