use log::{error, info};
use serenity::Client;
use serenity::client::bridge::gateway::GatewayIntents;
use serenity::framework::StandardFramework;

mod commands;
mod config;
mod handler;
mod util;

const NAME: &'static str = env!("CARGO_PKG_NAME");
const VERSION: &'static str = env!("CARGO_PKG_VERSION");

#[tokio::main]
async fn main() {
    env_logger::init();

    info!("{} v{}. Starting...", NAME, VERSION);

    let bot_token = config::token();

    let framework = StandardFramework::new()
        .configure(|c| c.prefix("!"))
        .group(&commands::EVENT_GROUP);

    let client = Client::builder(bot_token)
        .intents(GatewayIntents::all())
        .event_handler(handler::Handler)
        .framework(framework)
        .await;

    if let Err(e) = client {
        error!("Client error: {}", e);
        return;
    }
    let mut client = client.unwrap();

    if let Err(e) = client.start().await {
        error!("Client error: {}", e);
        return;
    }
}
