use serenity::model::id::ChannelId;

static DISCORD_TOKEN: &str = "DISCORD_TOKEN";
static CHANNEL_WELCOME: &str = "CHANNEL_WELCOME";
static CHANNEL_MESSAGE_LOG: &str = "CHANNEL_MESSAGE_LOG";

pub fn token() -> String {
    let bot_token = std::env::var(DISCORD_TOKEN);
    if let Err(e) = bot_token {
        panic!("Couldn't retrieve token from environment: {}", e);
    }
    bot_token.unwrap()
}

pub fn welcome_channel() -> ChannelId {
    let channel_id = std::env::var(CHANNEL_WELCOME);
    if let Err(e) = channel_id {
        panic!("Couldn't retrieve token from environment: {}", e);
    }
    let channel_id = channel_id.unwrap().parse();

    if let Err(e) = channel_id {
        panic!("Couldn't parse welcome channel id: {}", e);
    }
    let channel_id = channel_id.unwrap();

    ChannelId(channel_id)
}

pub fn message_log_channel() -> ChannelId {
    let channel_id = std::env::var(CHANNEL_MESSAGE_LOG);
    if let Err(e) = channel_id {
        panic!("Couldn't retrieve token from environment: {}", e);
    }
    let channel_id = channel_id.unwrap().parse();

    if let Err(e) = channel_id {
        panic!("Couldn't parse welcome channel id: {}", e);
    }
    let channel_id = channel_id.unwrap();

    ChannelId(channel_id)
}
